import { DataQuery, DataSourceJsonData } from '@grafana/data';

export interface MyQuery extends DataQuery {
  queryText: string;
  scroll: boolean;
}

export const defaultQuery: Partial<MyQuery> = {
  queryText: '{\n\
  "query": {\n\
    "bool": {\n\
      "must": [\n\
        "range": {\n\
          "$__timeField()": {\n\
            "gte": "$__timeFrom()",\n\
            "lte": "$__timeTo()" \n\
          }\n\
        }\n\
      ]\n\
    }\n\
  }\n\
}',
  scroll: false,
};

export interface ElasticDatasourceOptions extends DataSourceJsonData {
  hosts: Array<string>;
  sslVerifyCerts: boolean;
  basicAuth: boolean;
  basicAuthPassword?: string;
  basicAuthUsername?: string;
  timeField: string;
  indexAlias: string;
}
