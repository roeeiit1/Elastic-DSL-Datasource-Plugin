import React, { PureComponent } from 'react';
import { DataSourcePluginOptionsEditorProps } from '@grafana/data';
import { ElasticDatasourceOptions } from './types';
import { SecutiyComponent } from 'components/configComponents/Security/SecutiyComponent';
import { BasicAuthController } from 'components/configComponents/BasicServerConnection/BasicConnectionComponent';
import { BasicElasticConfig } from 'components/configComponents/ElasticConfiguraiton/BasicElasticComponent';

interface Props extends DataSourcePluginOptionsEditorProps<ElasticDatasourceOptions> { }

interface State { }

export class ConfigEditor extends PureComponent<Props, State> {
  constructor(props: Props) {
    //default constructor
    super(props);

    // set defualts data
    const { options, onOptionsChange } = this.props;
    options.jsonData.sslVerifyCerts = options.jsonData.sslVerifyCerts || false;
    options.jsonData.basicAuth = options.jsonData.basicAuth || false;
    options.jsonData.hosts = (this.props.options.jsonData.hosts === undefined || this.props.options.jsonData.hosts === []) ? [""] : this.props.options.jsonData.hosts;
    options.jsonData.timeField = options.jsonData.timeField || '@timestamp';
    onOptionsChange({
      ...options
    });
  }

  render() {
    return (
      <div>
        <BasicAuthController
          {...this.props}
        />
        <div style={{ margin: "25px" }} />
        <SecutiyComponent
          {...this.props}
        />
        <div style={{ margin: "25px" }} />
        <BasicElasticConfig
          {...this.props}
        />
      </div>
    );
  }
}
