import defaults from 'lodash/defaults';
import axios, { AxiosRequestConfig } from 'axios';
import { ClientOptions } from '@elastic/elasticsearch';
import {
  DataQueryRequest,
  DataQueryResponse,
  DataSourceApi,
  DataSourceInstanceSettings,
  MutableDataFrame,
  FieldType,
} from '@grafana/data';
import { MyQuery, ElasticDatasourceOptions, defaultQuery } from './types';
import moment from 'moment';

function flatten(target: object, opts?: { delimiter?: any; maxDepth?: any; safe?: any }): any {
  opts = opts || {};

  const delimiter = opts.delimiter || '.';
  let maxDepth = opts.maxDepth || 10;
  let currentDepth = 1;
  const output: any = {};

  function step(object: any, prev: string | null) {
    Object.keys(object).forEach(key => {
      const value = object[key];
      const isarray = opts?.safe && Array.isArray(value);
      const type = Object.prototype.toString.call(value);
      const isobject = type === '[object Object]';

      const newKey = prev ? prev + delimiter + key : key;

      if (!opts?.maxDepth) {
        maxDepth = currentDepth + 1;
      }

      if (!isarray && isobject && Object.keys(value).length && currentDepth < maxDepth) {
        ++currentDepth;
        return step(value, newKey);
      }

      output[newKey] = value;
    });
  }

  step(target, null);

  return output;
}

type Doc = {
  _id: string;
  _type: string;
  _index: string;
  _source?: any;
};

export class DataSource extends DataSourceApi<MyQuery, ElasticDatasourceOptions> {
  settings: ElasticDatasourceOptions;
  esClientSettings: ClientOptions;

  constructor(instanceSettings: DataSourceInstanceSettings<ElasticDatasourceOptions>) {
    super(instanceSettings);

    this.settings = instanceSettings.jsonData;
    this.esClientSettings = {
      nodes: this.settings.hosts,
      ssl: {
        rejectUnauthorized: this.settings.sslVerifyCerts,
      },
    };

    if (this.settings.basicAuth)
      this.esClientSettings.auth = {
        username: this.settings.basicAuthUsername || '',
        password: this.settings.basicAuthPassword || '',
      };
  }

  flattenHits = (hits: Doc[]): { docs: Array<Record<string, any>>; propNames: string[] } => {
    const docs: any[] = [];
    let propNames: string[] = [];

    for (const hit of hits) {
      const flattened = hit._source ? flatten(hit._source) : {};
      const doc = {
        ...flattened,
      };

      for (const propName of Object.keys(doc)) {
        if (propNames.indexOf(propName) === -1) {
          propNames.push(propName);
        }
      }

      docs.push(doc);
    }

    propNames.sort();
    return { docs, propNames };
  };

  private getAxiosConfig(): AxiosRequestConfig | {} {
    if (this.settings.basicAuth)
      return {
        auth: {
          username: this.settings.basicAuthUsername || '',
          password: this.settings.basicAuthPassword || '',
        },
        timeout: 1000,
      };
    return {};
  }

  private async getActiveServer(): Promise<string> {
    const nodesInfoPostfix = '/' + this.settings.indexAlias + '/_mapping';
    let activeHost = '';
    const sortedHosts = this.settings.hosts.sort((a, b) => {
      return 0.5 - Math.random();
    });
    for (let i = 0; i < sortedHosts.length; i++) {
      const host = sortedHosts[i];
      activeHost =
        (await axios
          .get(host + nodesInfoPostfix, this.getAxiosConfig())
          .then(res => {
            if (res.status === 200) return host;
            else return '';
          })
          .catch(err => {
            return '';
          })) || '';
      if (activeHost !== '') return activeHost;
    }
    throw new Error('Couldnt Find Active Server');
  }

  private async getMappingFields(): Promise<{ name: string; type: FieldType }[]> {
    let fieldsArr: { name: string; type: FieldType }[] = [];
    const activeHost = await this.getActiveServer();
    const nodesInfoPostfix = '/' + this.settings.indexAlias + '/_mapping';
    const res = await axios.get(activeHost + nodesInfoPostfix, this.getAxiosConfig());
    let fields = await flatten(res.data[this.settings.indexAlias].mappings.properties);
    Object.entries(fields).forEach(entry => {
      if (entry[0].includes('.type')) {
        let type;
        switch (entry[1]) {
          case 'date':
            type = FieldType.time;
            break;
          case 'text':
            type = FieldType.string;
            break;
          case 'keyword':
            type = FieldType.string;
            break;
          case 'number':
            type = FieldType.number;
            break;
          case 'boolean':
            type = FieldType.boolean;
            break;
          default:
            type = FieldType.other;
        }
        fieldsArr.push({
          name: entry[0].split('.type')[0],
          type: type,
        });
      }
    });
    return fieldsArr;
  }

  private convertTextMacros(queryText: string, fromTime: number, toTime: number): string {
    queryText = queryText.replace("$__timeField()", this.settings.timeField);
    queryText = queryText.replace("$__timeFrom()", moment(fromTime, 'X').format);
    queryText = queryText.replace("$__timeTo()", moment(toTime, 'X').format);
    return queryText
  }

  private async queryElasticSearchNonScroll(queryText: string, from: number, to: number) {
    const activeHost = await this.getActiveServer();
    const searchPostfix = '/' + this.settings.indexAlias + '/_search';
    queryText = this.convertTextMacros(queryText, from, to);
    let body = JSON.parse(queryText);
    console.log(body);
    return axios.post(activeHost + searchPostfix, body, this.getAxiosConfig()).then(res => {
      return this.flattenHits(res.data.hits.hits);
    });
  }

  private async queryElasticSearchScroll(queryText: string, from: number, to: number) {
    const activeHost = await this.getActiveServer();
    const searchPostfix = '/' + this.settings.indexAlias + '/_search?scroll=10m';
    const scrollPostfix = '/_search/scroll';
    queryText = this.convertTextMacros(queryText, from, to);
    let body = JSON.parse(queryText);
    console.log(body);
    const hits: any[] = [];
    let baseHits: any[] = [];
    let scroll_id: string = '';
    let { docs, propNames } = await axios.post(activeHost + searchPostfix, body, this.getAxiosConfig()).then(res => {
      baseHits = res.data.hits.hits;
      scroll_id = res.data._scroll_id;
      let { docs, propNames } = this.flattenHits(baseHits);
      return { docs, propNames };
    });
    hits.push(...docs);
    while (baseHits !== undefined && baseHits.length > 0) {
      let { docs } = await axios
        .post(activeHost + scrollPostfix, { scroll_id: scroll_id }, this.getAxiosConfig())
        .then((res): { docs: Array<Record<string, any>>; propNames: string[] } => {
          baseHits = res.data.hits.hits;
          return ({ docs, propNames } = this.flattenHits(baseHits));
        });
      hits.push(...docs);
    }
    axios.delete(activeHost + scrollPostfix, { ...this.getAxiosConfig(), data: { scroll_id: scroll_id } }).then(res => {
      console.log(res);
    });
    return { docs: hits, propNames: propNames };
  }

  private async queryElasticSearch(
    queryText: string,
    from: number,
    to: number,
    scrolling: boolean
  ): Promise<{ docs: Array<Record<string, any>>; propNames: string[] }> {
    return scrolling
      ? this.queryElasticSearchScroll(queryText, from, to)
      : this.queryElasticSearchNonScroll(queryText, from, to);
  }

  async query(options: DataQueryRequest<MyQuery>): Promise<DataQueryResponse> {
    const { range } = options;
    const from = range!.from.valueOf();
    const to = range!.to.valueOf();
    const fields = await this.getMappingFields();

    const data = options.targets.map(async target => {
      const query = defaults(target, defaultQuery);
      const { docs } = await this.queryElasticSearch(query.queryText, from, to, query.scroll);
      let frame = new MutableDataFrame({
        refId: query.refId,
        fields: fields,
      });
      for (const doc in docs) {
        frame.add({ ...docs[doc] }, true);
      }
      return frame;
    });
    return { data };
  }

  async testDatasource() {
    return { status: 'success', message: 'Success' };
  }
}
