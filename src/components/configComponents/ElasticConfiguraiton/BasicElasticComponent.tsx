import React, { PureComponent } from 'react';
import { LegacyForms } from '@grafana/ui';
import { DataSourcePluginOptionsEditorProps } from '@grafana/data';
import { ElasticDatasourceOptions } from '../../../types';

const { FormField } = LegacyForms;

interface Props extends DataSourcePluginOptionsEditorProps<ElasticDatasourceOptions> { }

interface State { }

export class BasicElasticConfig extends PureComponent<Props, State> {
  render() {
    return (
      <div>
        <div className="gf-form">
          <FormField
            label="Index Alias"
            value={this.props.options.jsonData.indexAlias}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              const { options, onOptionsChange } = this.props;
              options.jsonData.indexAlias = event.target.value;
              onOptionsChange({ ...options });
            }}
          />
        </div>
        <div className="gf-form">
          <FormField
            label="Time Field"
            value={this.props.options.jsonData.timeField}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              const { options, onOptionsChange } = this.props;
              options.jsonData.timeField = event.target.value;
              onOptionsChange({ ...options });
            }}
          />
        </div>
      </div>
    );
  }
}
