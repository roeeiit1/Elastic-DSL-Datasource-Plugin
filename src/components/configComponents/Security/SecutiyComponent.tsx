import React, { SyntheticEvent, PureComponent } from 'react';
import { LegacyForms, ModalHeader } from '@grafana/ui';
import { DataSourcePluginOptionsEditorProps } from '@grafana/data';
import { ElasticDatasourceOptions } from '../../../types';
import { BasicAuthController } from './BasicAuthComponent';

const { Switch } = LegacyForms;

interface Props extends DataSourcePluginOptionsEditorProps<ElasticDatasourceOptions> { }

interface State { }

export class SecutiyComponent extends PureComponent<Props, State> {

  onVerifyCertChange = (event: SyntheticEvent<HTMLInputElement, Event>) => {
    let { onOptionsChange, options } = this.props;
    options.jsonData.sslVerifyCerts = !options.jsonData.sslVerifyCerts;
    onOptionsChange({ ...options });
  };

  render() {
    const { options } = this.props;

    return (
      <div>
        <div className="gf-form">
          <ModalHeader
            title="Security Configuration"
            icon="key-skeleton-alt"
          />
        </div>
        <div className="gf-form-group">
          <BasicAuthController
            {...this.props}
          />
          <div
            style={{ margin: "10px" }}
          />
          <div className="gf-form">
            <Switch
              label="Verify SSL"
              onChange={this.onVerifyCertChange}
              checked={options.jsonData.sslVerifyCerts}
              tooltip="Verify SSL Certificaties"
            />
          </div>
        </div >
      </div>
    );
  }
}
