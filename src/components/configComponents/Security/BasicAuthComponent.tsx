import React, { SyntheticEvent, ChangeEvent, PureComponent } from 'react';
import { LegacyForms } from '@grafana/ui';
import { DataSourcePluginOptionsEditorProps } from '@grafana/data';
import { ElasticDatasourceOptions } from '../../../types';

const { FormField, Switch } = LegacyForms;

interface Props extends DataSourcePluginOptionsEditorProps<ElasticDatasourceOptions> { }

interface State { }

export class BasicAuthController extends PureComponent<Props, State> {
  onBasicAuthChange = (event: SyntheticEvent<HTMLInputElement, Event>) => {
    let { onOptionsChange, options } = this.props;
    options.jsonData.basicAuth = !options.jsonData.basicAuth;
    if (options.jsonData.basicAuth && options.jsonData.basicAuthPassword === undefined)
      options.jsonData.basicAuthPassword = "";
    if (options.jsonData.basicAuth && options.jsonData.basicAuthUsername === undefined)
      options.jsonData.basicAuthUsername = "";
    onOptionsChange({ ...options });
  };

  onPasswordChange = (event: ChangeEvent<HTMLInputElement>) => {
    let { onOptionsChange, options } = this.props;
    options.jsonData.basicAuthPassword = event.target.value;
    onOptionsChange({ ...options });
  };

  onUsernameChange = (event: ChangeEvent<HTMLInputElement>) => {
    let { onOptionsChange, options } = this.props;
    options.jsonData.basicAuthUsername = event.target.value;
    onOptionsChange({ ...options });
  };

  render() {
    const { options } = this.props;
    return (
      <div>
        <div className="gf-form">
          <Switch
            label="Basic Authentication"
            onChange={this.onBasicAuthChange}
            checked={options.jsonData.basicAuth}
            tooltip="Http Basic Authentication"
          />
        </div>
        <div className="gf-form" style={{ display: !options.jsonData.basicAuth ? "none" : "block" }}>
          <FormField
            tooltip="Elasticsearch Basic Authentication Username"
            label="Username"
            labelWidth={6}
            inputWidth={6}
            onChange={this.onUsernameChange}
            value={options.jsonData.basicAuthUsername}
          />
        </div>
        <div className="gf-form" style={{ display: !options.jsonData.basicAuth ? "none" : "block" }}>
          <FormField
            tooltip="Elasticsearch Basic Authentication Password"
            label="Password"
            labelWidth={6}
            inputWidth={6}
            onChange={this.onPasswordChange}
            value={options.jsonData.basicAuthPassword}
          />
        </div>
      </div>
    );
  }
}
