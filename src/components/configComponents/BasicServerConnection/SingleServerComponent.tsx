import React, { ChangeEvent, PureComponent } from 'react';
import { LegacyForms, Button } from '@grafana/ui';

const { FormField } = LegacyForms;

interface Props {
    hostname: string;
    onDelete: { (event: React.MouseEvent<HTMLButtonElement, MouseEvent>): void };
    onChange: { (event: ChangeEvent<HTMLInputElement>): void };
    displayDelete: boolean;
}

interface State { }

export class SingleServer extends PureComponent<Props, State> {
    render() {
        const { hostname, onDelete, onChange, displayDelete } = this.props;

        return (
            <>
                <FormField
                    tooltip="Hostname including HTTP Schema and Port"
                    label="Hostname"
                    placeholder="http://localhost:9200"
                    labelWidth={6}
                    inputWidth={20}
                    onChange={onChange}
                    value={hostname}
                />
                <Button
                    onClick={onDelete}
                    style={{ marginLeft: "10px", display: displayDelete ? "none" : "block" }}
                >
                    Delete Host
                </Button>
            </>
        );
    }
}
