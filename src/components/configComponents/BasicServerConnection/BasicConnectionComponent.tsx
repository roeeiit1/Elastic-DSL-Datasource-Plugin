import React, { ChangeEvent, PureComponent } from 'react';
import { ModalHeader, Button } from '@grafana/ui';
import { DataSourcePluginOptionsEditorProps } from '@grafana/data';
import { ElasticDatasourceOptions } from '../../../types';
import { SingleServer } from './SingleServerComponent';

interface Props extends DataSourcePluginOptionsEditorProps<ElasticDatasourceOptions> { }

interface State { }

export class BasicAuthController extends PureComponent<Props, State> {
    render() {
        const { onOptionsChange, options } = this.props;
        let hostArray: Array<JSX.Element> = [];
        (options.jsonData.hosts || []).forEach((host: string) => {
            hostArray.push(
                <div className="gf-form" style={{ marginBottom: "10px" }}>
                    <SingleServer
                        hostname={(options.jsonData.hosts || [])[(options.jsonData.hosts || []).indexOf(host)]}
                        onDelete={(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
                            if ((options.jsonData.hosts || []) !== [])
                                (options.jsonData.hosts || []).splice((options.jsonData.hosts || []).indexOf(host), 1);
                        }}
                        onChange={(event: ChangeEvent<HTMLInputElement>) => {
                            if ((options.jsonData.hosts || []) !== [])
                                (options.jsonData.hosts || [])[(options.jsonData.hosts || []).indexOf(host)] = event.target.value;
                            onOptionsChange({ ...options });
                        }}
                        displayDelete={(options.jsonData.hosts || []).length === 1}
                    />
                </div>
            );
        });
        return (
            <>
                <div className="gf-form">
                    <ModalHeader
                        title="Host \ Server Configuration"
                        icon="power"
                    />
                </div>
                {hostArray}
                <div className="gf-form">
                    <Button
                        onClick={(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
                            options.jsonData.hosts = (options.jsonData.hosts || []);
                            options.jsonData.hosts.push("");
                            onOptionsChange({ ...options });
                        }}
                    >
                        Add Host
                    </Button>
                </div>
            </>
        );
    }
}
