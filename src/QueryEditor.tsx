import defaults from 'lodash/defaults';

import React, { PureComponent } from 'react';
import { QueryField, LegacyForms, InlineFormLabel } from '@grafana/ui';
import { QueryEditorProps } from '@grafana/data';
import { DataSource } from './DataSource';
import { defaultQuery, ElasticDatasourceOptions, MyQuery } from './types';
const { Switch } = LegacyForms;
type Props = QueryEditorProps<DataSource, MyQuery, ElasticDatasourceOptions>;

export class QueryEditor extends PureComponent<Props> {

  onConstantChange = (event: string) => {
    const { onChange, query, onRunQuery } = this.props;
    onChange({ ...query, queryText: event });
    onRunQuery();
  };

  onScrollingChange = (event: React.SyntheticEvent<HTMLInputElement, Event>) => {
    const { onChange, query, onRunQuery } = this.props;
    onChange({ ...query, scroll: !query.scroll });
    onRunQuery();
  }

  render() {
    const query = defaults(this.props.query, defaultQuery);

    return (
      <>
        <div className="gf-form">
          <InlineFormLabel
            tooltip={
              <a href="https://bit.ly/3flm6Ju" target="_">
                DSL Search Query
              </a>}
            width="auto">
            Query Text
          </InlineFormLabel>
          <QueryField
            portalOrigin=""
            onChange={this.onConstantChange}
            query={query.queryText}
          />
        </div>
        <div className="gf-form">
          <Switch
            label="Scrolling"
            checked={query.scroll}
            onChange={this.onScrollingChange}
          />
        </div>
      </>
    );
  }
}
